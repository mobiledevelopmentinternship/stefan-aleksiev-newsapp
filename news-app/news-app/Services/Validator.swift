//
//  EmailValidator.swift
//  news-app
//
//  Created by Stefan Aleksiev on 8.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

class Validator: ValidateFields {
    func validateEmail(email: String) -> Bool {
        return isEmailValid(email: email)
    }
    
    func validatePassword(password: String) -> Bool {
        return isPasswordValid(password: password)
    }
    
    func validateAuthor(author: String) -> String {
        guard let detector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue) else { return "" }
        let matches = detector.matches(in: author, options: [], range: NSRange(location: 0, length: author.utf16.count))
        return matches.isEmpty ? author : ""
    }
}
