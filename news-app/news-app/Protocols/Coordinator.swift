//
//  Coordinator.swift
//  news-app
//
//  Created by Stefan Aleksiev on 1.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

// MARK: - Coordinator Protocols
protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: CustomNavigationController { get set }
    func start()
}

protocol CoordinatorStarted {
    var worldNewsStarted: Bool { get set }
    var newsPaperStarted: Bool { get set }
    var favouritesStarted: Bool { get set }
}
