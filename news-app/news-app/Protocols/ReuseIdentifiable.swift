//
//  ReuseIdentifiable.swift
//  news-app
//
//  Created by Stefan Aleksiev on 15.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

protocol ReuseIdentifiable {
    static var reuseIdentifier: String { get }
}

extension ReuseIdentifiable {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UICollectionViewCell: ReuseIdentifiable {}
extension UITableViewCell: ReuseIdentifiable {}
