//
//  CoordinatorNavigation.swift
//  news-app
//
//  Created by Stefan Aleksiev on 5.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

protocol CoordinatorNavigation {
    var worldNewsNavigation: UINavigationController { get set }
    var newsPaperNavigation: UINavigationController { get set }
    var favouriteNavigation: UINavigationController { get set }
}
