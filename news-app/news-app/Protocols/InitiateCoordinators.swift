//
//  InitiateCoordinators.swift
//  news-app
//
//  Created by Stefan Aleksiev on 5.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

protocol InitiateCoordinators {
    func startWorldNews()
    func startNewsPaper()
    func startFavourites()
}
