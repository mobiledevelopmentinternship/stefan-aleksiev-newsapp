//
//  Navigation.swift
//  news-app
//
//  Created by Stefan Aleksiev on 8.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

// MARK: - Common Protocols
protocol BackwardsNavigable: class {
    func navigateBack(to coordinator: Coordinator)
}
