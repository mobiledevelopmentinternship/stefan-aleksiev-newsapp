//
//  RegisterVM.swift
//  news-app
//
//  Created by Stefan Aleksiev on 2.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import FirebaseAuth

class RegisterViewModel {
    
    // MARK: - Properties
    private var email: String {
        return user.email ?? ""
    }
    
    private var password: String {
        return user.password ?? ""
    }
    
    private let user: User
    
    // MARK: - Init
    init (user: User) {
        self.user = user
    }
}

extension RegisterViewModel {
    func register(email:String, password: String, completionHandler: @escaping AuthCompletionHandler) {
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            guard error != nil else {
                completionHandler(.sucess(result))
                return
            }
            guard let err = error else { return }
            completionHandler(.failure(err))
        }
    }
}
