//
//  LoginVM.swift
//  news-app
//
//  Created by Stefan Aleksiev on 2.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import FirebaseAuth

typealias AuthCompletionHandler = ((AuthResult<User>)) -> ()

class LoginViewModel {
    
    private var email: String {
        return user.email ?? ""
    }
    
    private var password: String {
        return user.password ?? ""
    }
    
    private let user: User
    
    init (user: User) {
        self.user = user
    }
}

extension LoginViewModel {
    func login(email: String, password: String, completionHandler: @escaping AuthCompletionHandler) {
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            guard result == nil else {
                completionHandler(.sucess(result))
                return
            }
            guard let err = error else { return }
            completionHandler(.failure(err))
        }
    }
}

enum AuthResult<User> {
    case sucess(AuthDataResult?)
    case failure(Error)
}
