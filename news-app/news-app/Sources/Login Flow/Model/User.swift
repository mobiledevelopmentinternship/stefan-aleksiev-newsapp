//
//  User.swift
//  news-app
//
//  Created by Stefan Aleksiev on 2.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

struct User {
    
    let email: String? = ""
    let password: String? = ""
}
