//
//  LoginViewController.swift
//  news-app
//
//  Created by Stefan Aleksiev on 3.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var loginButton: UIButton!
    @IBOutlet private weak var registerButton: UIButton!
    
    // MARK: - Validation IBOutlets
    @IBOutlet private weak var validateEmailLabel: UILabel!
    @IBOutlet private weak var validatePasswordLabel: UILabel!
        
    // MARK: - Delegates
    var coordinator: LoginCoordinator
    
    // MARK: - Properties
    private let viewModel: LoginViewModel
    private var validator = Validator()
    
    // MARK: - Init
    init(coordinator: LoginCoordinator, viewModel: LoginViewModel) {
        self.coordinator = coordinator
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
}

// MARK: - Setup
private extension LoginViewController {
    func setup() {
        configureLoginBtn(state: false)
        setupValidationFields()
        setFonts()
    }
    
    func configureLoginBtn(state: Bool) {
        loginButton.isEnabled = state
        loginButton.alpha = state ? 1.0 : 0.5
    }
    
    func setupValidationFields() {
        validateEmailLabel.text = Constants.ValidationErrors.email
        validatePasswordLabel.text = Constants.ValidationErrors.password
        
        validateEmailLabel.isHidden = true
        validatePasswordLabel.isHidden = true
        
        emailTextField.addTarget(self, action: #selector(validateEmail), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(validatePassword), for: .editingChanged)
    }
    
    func setFonts() {
        emailTextField.font = Font.regular.of(size: .medium)
        passwordTextField.font = Font.regular.of(size: .medium)
        
        validateEmailLabel.font = Font.regular.of(size: .semiMedium)
        validatePasswordLabel.font = Font.regular.of(size: .semiMedium)
        
        loginButton.titleLabel?.font = Font.bebasBold.of(size: .medium)
        registerButton.titleLabel?.font = Font.bebasBold.of(size: .medium)
    }
}

// MARK: - Actions
private extension LoginViewController {
    @IBAction func login(_ sender: Any) {
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        
        viewModel.login(email: email, password: password) { (result) in
            switch result {
            case .sucess( _):
                self.coordinator.startTabCoordinator()
            case .failure( _):
                if !Connectivity.shared.isConnected { self.showAlert(title: Constants.Error.title, message: Constants.Error.noInternetConnection)}
                self.showAlert(title: Constants.Error.title, message: Constants.Error.Login.message)
            }
        }
    }
    
    @IBAction func register(_ sender: Any) {
        coordinator.startRegisterCoordinator()
    }
}

// MARK: - Validation
private extension LoginViewController {
    @objc private func validateEmail() -> Bool {
        guard let email = emailTextField.text else { return false }
        guard validator.validateEmail(email: email) else {
            configureLoginBtn(state: false)
            return validationConfig(label: validateEmailLabel, state: false)
        }
        allPropertiesAreValid()
        return validationConfig(label: validateEmailLabel, state: true)
    }
    
    @objc private func validatePassword() -> Bool {
        guard let password = passwordTextField.text else { return false }
        guard validator.validatePassword(password: password) else {
            configureLoginBtn(state: false)
            return validationConfig(label: validatePasswordLabel,state: false)
        }
        allPropertiesAreValid()
        return validationConfig(label: validatePasswordLabel, state: true)
    }
    
    func validationConfig(label: UILabel, state: Bool) -> Bool {
        label.isHidden = state
        return state
    }
    
    func allPropertiesAreValid() {
        guard
            let email = emailTextField.text,
            let password = passwordTextField.text
        else { return }
        
        let emailValidation = validator.validateEmail(email: email)
        let passwordValidation = validator.validatePassword(password: password)
        if emailValidation && passwordValidation { configureLoginBtn(state: true) }
    }
}

// MARK: - Delegates
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        emailTextField.endEditing(true)
        passwordTextField.endEditing(true)
        return false
    }
}
