//
//  RegisterViewController.swift
//  news-app
//
//  Created by Stefan Aleksiev on 1.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit
import iOSDropDown

class RegisterViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var repeatPasswordTextField: UITextField!
    @IBOutlet private weak var registerBtn: UIButton!
    
    // MARK: - Validation IBOutlets
    @IBOutlet private weak var validateEmailLabel: UILabel!
    @IBOutlet private weak var validatePasswordLabel: UILabel!
    @IBOutlet private weak var validateRepeatPasswordLabel: UILabel!
    
    // MARK: - Delegates
    var coordinator: RegisterCoordinator?
    
    // MARK: - Properties
    private let viewModel: RegisterViewModel
    private var validator = Validator()
    private let factory = Factory()
    
    init(coordinator: RegisterCoordinator, viewModel: RegisterViewModel) {
        self.coordinator = coordinator
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
}

// MARK: - Actions
private extension RegisterViewController {
    @IBAction func register(_ sender: Any) {
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        
        viewModel.register(email: email, password: password) { (result) in
            switch result {
            case .sucess( _):
                self.coordinator?.startMainTabCoordinator()
            case .failure( _):
                self.showAlert(title: Constants.Error.title, message: Constants.Error.Register.message)
            }
        }
    }
}

// MARK: - Setup
private extension RegisterViewController {
    func setup() {
        configureRegisterBtn(with: 0.5, state: false)
        setBackButton()
        setupValidationFields()
        setFonts()
    }
    
    func configureRegisterBtn(with alpha: CGFloat, state: Bool) {
        registerBtn.alpha = alpha
        registerBtn.isEnabled = state
    }
    
    func setupValidationFields() {
        validateEmailLabel.text = Constants.ValidationErrors.email
        validatePasswordLabel.text = Constants.ValidationErrors.password
        validateRepeatPasswordLabel.text = Constants.ValidationErrors.repeatPassword
        
        validateEmailLabel.isHidden = true
        validatePasswordLabel.isHidden = true
        validateRepeatPasswordLabel.isHidden = true
        
        emailTextField.addTarget(self, action: #selector(validateEmail), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(validatePassword), for: .editingChanged)
        repeatPasswordTextField.addTarget(self, action: #selector(validateRepeatPassword), for: .editingChanged)
    }
    
    func setFonts() {
        emailTextField.font = Font.regular.of(size: .medium)
        passwordTextField.font = Font.regular.of(size: .medium)
        repeatPasswordTextField.font = Font.regular.of(size: .medium)
        
        validateEmailLabel.font = Font.regular.of(size: .semiMedium)
        validatePasswordLabel.font = Font.regular.of(size: .semiMedium)
        validateRepeatPasswordLabel.font = Font.regular.of(size: .semiMedium)
        
        registerBtn.titleLabel?.font = Font.bebasBold.of(size: .medium)
    }
}

// MARK: - Validation
private extension RegisterViewController  {
    @objc private func validateEmail() -> Bool {
        guard let email = emailTextField.text else { return false }
        guard validator.validateEmail(email: email) else {
            configureRegisterBtn(with: 0.5, state: false)
            return validationConfig(label: validateEmailLabel, state: false)
        }
        allPropertiesAreValid()
        return validationConfig(label: validateEmailLabel, state: true)
    }
    
    @objc private func validatePassword() -> Bool  {
        guard let password = passwordTextField.text else { return false }
        guard validator.validatePassword(password: password) else {
            configureRegisterBtn(with: 0.5, state: false)
            return validationConfig(label: validatePasswordLabel, state: false)
        }
        allPropertiesAreValid()
        return validationConfig(label: validatePasswordLabel, state: true)
    }
    
    @objc private func validateRepeatPassword() -> Bool  {
        guard let currentPass = passwordTextField.text, let newPass = repeatPasswordTextField.text else { return false }
        guard currentPass == newPass else {
            configureRegisterBtn(with: 0.5, state: false)
            return validationConfig(label: validateRepeatPasswordLabel, state: false)
        }
        allPropertiesAreValid()
        return validationConfig(label: validateRepeatPasswordLabel, state: true)
    }
    
    func validationConfig(label: UILabel, state: Bool) -> Bool {
        label.isHidden = state
        return state
    }
    
    func allPropertiesAreValid() {
        guard
            let email = emailTextField.text,
            let password = passwordTextField.text,
            let repeatPassword = repeatPasswordTextField.text
        else { return }
        
        let emailValidation = validator.validateEmail(email: email)
        let passwordValidation = validator.validatePassword(password: password)
        let repeatPasswordValidation = password == repeatPassword
        
        if emailValidation && passwordValidation && repeatPasswordValidation {
            configureRegisterBtn(with: 1, state: true)
        }
    }
}

// MARK: - Navigation
private extension RegisterViewController {
    func setBackButton() {
        self.navigationItem.leftBarButtonItem = factory.setNavigationButtonItem(self, action: #selector(didNavigateBack), iconPath: Constants.NavigationItem.Back.icon)
    }
    
    @objc func didNavigateBack() {
        coordinator?.didNavigateBack()
    }
}

// MARK: - Delegates
extension RegisterViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        emailTextField.endEditing(true)
        passwordTextField.endEditing(true)
        repeatPasswordTextField.endEditing(true)
        return false
    }
}
