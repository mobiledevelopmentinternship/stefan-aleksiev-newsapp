//
//  FavouriteViewController.swift
//  news-app
//
//  Created by Stefan Aleksiev on 2.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class FavouriteViewController: UIViewController {
    
    // MARK: - Coordinator
    var coordinator: FavouritesCoordinator
    
    // MARK: - IBOutlets
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var favouritesLabel: UILabel!
    
    // MARK: - Properties
    private let viewModel: FavouritesViewModel
    
    // MARK: - Init
    init(coordinator: FavouritesCoordinator, viewModel: FavouritesViewModel) {
        self.coordinator = coordinator
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        self.collectionView.reloadData()
    }
}

// MARK: - Setup
private extension FavouriteViewController {
    func setup() {
        title = Constants.Titles.favourite
        viewModel.articles = coordinator.fetchArticles()
        setupCollectionView()
    }
    
    func setupCollectionView() {
        setRechabilityLabel()
        collectionView.register(UINib(nibName: FavouriteCollectionViewCell.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: FavouriteCollectionViewCell.reuseIdentifier)
        collectionView.dataSource = self
        collectionView.delegate = self
        ActivityIndicatorManager.shared.stop()
    }
}

// MARK: - Additional Functions
private extension FavouriteViewController {
    func setRechabilityLabel() {
        if viewModel.articles.count == 0 {
            favouritesLabel.text = Constants.Labels.emptyFavourites
            favouritesLabel.center = self.view.center
            favouritesLabel.font = Font.bebasBold.of(size: .large)
            favouritesLabel.textColor = Colors.secondComplimentary
            favouritesLabel.isHidden = false
        } else { favouritesLabel.isHidden = true }
    }
}
// MARK: - Delegates Data Source
extension FavouriteViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.articles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FavouriteCollectionViewCell.reuseIdentifier, for: indexPath) as? FavouriteCollectionViewCell else { return UICollectionViewCell()}
        
        cell.configure(with: viewModel.articles[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        coordinator.startNewsDetails(articleModel: viewModel.articles[indexPath.row])
    }
}

// MARK: - Delegates Style
extension FavouriteViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height / 5)
    }
}
