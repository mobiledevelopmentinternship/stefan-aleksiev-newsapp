//
//  NewsDetailsViewController.swift
//  news-app
//
//  Created by Stefan Aleksiev on 4.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit
import MXParallaxHeader
import RealmSwift

class NewsDetailsViewController: UIViewController {
    
    // MARK: - Coordinator
    weak var coordinator: NewsDetailsCoordinator?
    
    // IBOutlets
    @IBOutlet private weak var source: UILabel!
    @IBOutlet private weak var author: UILabel!
    @IBOutlet private weak var publishAt: UILabel!
    @IBOutlet private weak var articleTitle: UILabel!
    @IBOutlet private weak var desc: UILabel!
    @IBOutlet private weak var content: UILabel!
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var readButton: UIButton!
    @IBOutlet private weak var container: UIView!
    @IBOutlet private weak var saveButton: UIButton!
    
    // MARK: - Properties
    private let viewModel: ArticleDetailsViewModel
    private let repository = RealmManager(repository: RealmClient())
    private let validator = Validator()
    private let factory = Factory()
    
    
    // MARK: - Init
    init(coordinator: NewsDetailsCoordinator, viewModel: ArticleDetailsViewModel) {
        self.coordinator = coordinator
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
        
    override func viewWillLayoutSubviews() {
        let height = (source.frame.height + articleTitle.frame.height + desc.frame.height + content.frame.height + readButton.frame.height) * Constants.scrollViewМultiplier
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: height)
    }
}

// MARK: - Actions
private extension NewsDetailsViewController {
    @IBAction func readButtonPressed(_ sender: Any) {
        guard let apiUrl = viewModel.article.url else {
            self.showAlert(title: Constants.Error.title, message: Constants.Error.invalidURL)
            return
        }
        
        if let url = URL(string: apiUrl) {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {

        if viewModel.isSaved(viewModel.article) {
            repository.removeArticleFromDB(errorHandler: { (error) in
                self.showAlert(title: Constants.Error.title, message: Constants.Error.removeErrorMessage) },
                model: viewModel.article)
            self.showSuccessDialog(title: Constants.Completion.title, message: Constants.Completion.removeMessage, popVC: true)
        } else {
            repository.saveArticleToDB(errorHandler: { (error) in
                self.showAlert(title: Constants.Error.title, message: Constants.Error.saveErrorMessage)
            }, model: viewModel.article)
            self.showSuccessDialog(title: Constants.Completion.title, message: Constants.Completion.saveMessage, popVC: false)
            saveButton.setImage(Constants.Images.isSaved, for: .normal)
        }
    }
}

// MARK: - Setup
private extension NewsDetailsViewController {
    func setup() {
        navigationItem.leftBarButtonItem = factory.setNavigationButtonItem(self, action: #selector(didNavigateBack), iconPath: Constants.NavigationItem.BackWithTitle.icon)
        configure()
        setFonts()
    }
    
    func setFonts() {
        
        source.font = Font.bold.of(size: .semiLarge)
        source.textColor = Colors.prime
        
        author.font = Font.semiBold.of(size: .medium)
        author.textColor = Colors.secondComplimentary
        
        publishAt.font = Font.semiBold.of(size: .semiMedium)
        publishAt.textColor = Colors.secondComplimentary
        
        articleTitle.font = Font.bold.of(size: .semiLarge)
        articleTitle.textColor = Colors.third
        
        desc.font = Font.regular.of(size: .medium)
        desc.textColor = Colors.third
        
        content.font = Font.regular.of(size: .medium)
        content.textColor = Colors.third
        
        readButton.titleLabel?.font = Font.bold.of(size: .medium)
        readButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
}

// MARK: - Navigation
private extension NewsDetailsViewController {
    @objc func didNavigateBack() {
        coordinator?.didNavigateBack()
    }
}

// MARK: - Configure
private extension NewsDetailsViewController {
    func configure() {
        let headerView = UIImageView()
        let height = (source.frame.height + articleTitle.frame.height + desc.frame.height + content.frame.height + readButton.frame.height) * 5.0
        container.frame.size.height = height
        headerView.contentMode = .scaleAspectFill
        
        scrollView.parallaxHeader.view = headerView
        scrollView.parallaxHeader.height = self.view.frame.height / 3.0
        scrollView.parallaxHeader.mode = .fill
        scrollView.parallaxHeader.minimumHeight = self.view.frame.height / 4
        
        articleTitle.text = viewModel.article.title
        author.text = validator.validateAuthor(author: viewModel.article.author ?? "")
        source.text = viewModel.article.source?.name
        desc.text = viewModel.article.desc
        content.text = viewModel.article.content
        publishAt.text = viewModel.article.publishedAt?.asDateString()
        viewModel.isSaved(viewModel.article) ? saveButton.setImage(Constants.Images.isSaved, for: .normal) : saveButton.setImage(Constants.Images.isNotSaved, for: .normal)
        
        guard let apiImage = viewModel.article.urlToImage else {
            headerView.image = Constants.placeHolderImage
            return
        }
        
        headerView.kf.setImage(with: URL(string: apiImage), placeholder: Constants.placeHolderImage)
    }
}
