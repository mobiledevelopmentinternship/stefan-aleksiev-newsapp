//
//  SettingsViewController.swift
//  news-app
//
//  Created by Stefan Aleksiev on 5.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit
import iOSDropDown

class ProfileViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var oldPasswordTextField: UITextField!
    @IBOutlet private weak var newPasswordTextField: UITextField!
    @IBOutlet private weak var repeatNewPasswordTextField: UITextField!
    @IBOutlet private weak var saveBtn: UIButton!
    
    // MARK: - Validation IBOutlets
    @IBOutlet private weak var validateEmailLabel: UILabel!
    @IBOutlet private weak var validateOldPasswordLabel: UILabel!
    @IBOutlet private weak var validateNewPasswordLabel: UILabel!
    @IBOutlet private weak var validateRepeatNewPasswordLabel: UILabel!
    
    // MARK: - Delegates
    var coordinator: ProfileCoordinator
    
    // MARK: - Properties
    private let validator = Validator()
    private let viewModel: ProfileViewModel
    
    // MARK: - Init
    init(coordinator: ProfileCoordinator, viewModel: ProfileViewModel) {
        self.coordinator = coordinator
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
}

// MARK: - Actions
private extension ProfileViewController {
    @IBAction func logoutBtnPressed(_ sender: Any) {
        viewModel.singOut { (error) in
            self.showAlert(title: Constants.Error.title, message: error.localizedDescription)
        }
        coordinator.startLoginCoordinator()
    }
    
    @objc func onDidReceiveStatus(_ notification: Notification) {
        setFields(with: Connectivity.shared.isConnected)
    }
}

// MARK: - Setup
private extension ProfileViewController {
    func setup() {
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveStatus(_:)), name: .didReceiveStatus, object: nil)
        title = Constants.Titles.profile
        configureSaveBtn(with: 0.5, state: false)
        setupValidationFields()
        setBackButton()
    }
    
    func configureSaveBtn(with alpha: CGFloat, state: Bool) {
        saveBtn.alpha = alpha
        saveBtn.isEnabled = state
    }
    
    func setupValidationFields() {
        emailTextField.text = viewModel.getUserEmail()
        
        validateEmailLabel.text = Constants.ValidationErrors.email
        validateOldPasswordLabel.text = Constants.ValidationErrors.password
        validateNewPasswordLabel.text = Constants.ValidationErrors.password
        validateRepeatNewPasswordLabel.text = Constants.ValidationErrors.repeatPassword
        
        validateEmailLabel.isHidden = true
        validateOldPasswordLabel.isHidden = true
        validateNewPasswordLabel.isHidden = true
        validateRepeatNewPasswordLabel.isHidden = true
        
        emailTextField.addTarget(self, action: #selector(validateEmail), for: .editingChanged)
        oldPasswordTextField.addTarget(self, action: #selector(validateOldPassword), for: .editingChanged)
        newPasswordTextField.addTarget(self, action: #selector(validateNewPassword), for: .editingChanged)
        repeatNewPasswordTextField.addTarget(self, action: #selector(validateRepeatPassword), for: .editingChanged)
    }
    
    func setBackButton() {
        let backButton: UIBarButtonItem = {
            let button = UIButton(type: .custom)
            button.frame = Constants.NavigationItem.frame
            
            let buttonImage = UIImage(named: Constants.NavigationItem.Back.icon)
            button.setImage(buttonImage, for: .normal)
            button.addTarget(self, action: #selector(didNavigateBack), for: .touchUpInside)
            
            return UIBarButtonItem(customView: button)
        }()
        self.navigationItem.leftBarButtonItem = backButton
    }
    
    func setFields(with state: Bool) {
        oldPasswordTextField.isHidden = !state
        newPasswordTextField.isHidden = !state
        repeatNewPasswordTextField.isHidden = !state
    }
}

// MARK: - Navigation
private extension ProfileViewController {
    @objc func didNavigateBack() {
        coordinator.didNavigateBack()
    }
}

// MARK: - Actions
private extension ProfileViewController {
    @IBAction func saveBtn(_ sender: Any) {
        guard let newEmail = emailTextField.text else { return }
        guard let newPassword = newPasswordTextField.text else { return }
        
        viewModel.updateUser(errorHandler: { (error) in
            self.showAlert(title: Constants.Error.title, message: error.localizedDescription)
            return
        }, newEmail: newEmail, password: newPassword)
        postConfigure()
    }
    
    func postConfigure() {
        emailTextField.text = ""
        oldPasswordTextField.text = ""
        newPasswordTextField.text = ""
        repeatNewPasswordTextField.text = ""
        showSuccessDialog(title: Constants.Completion.title, message: Constants.Completion.updateMessage, popVC: true)
    }
}

// MARK: - Validation
private extension ProfileViewController {
    @objc private func validateEmail() -> Bool {
        guard let email = emailTextField.text else { return false }
        guard validator.validateEmail(email: email) else {
            configureSaveBtn(with: 0.5, state: false)
            return validationConfig(label: validateEmailLabel, state: false)
        }
        allPropertiesAreValid()
        return validationConfig(label: validateEmailLabel, state: true)
    }
    
    @objc private func validateOldPassword() -> Bool {
        guard let email = emailTextField.text else { return false }
        guard let password = oldPasswordTextField.text else { return false }
        guard validator.validatePassword(password: password) else {
            configureSaveBtn(with: 0.5, state: false)
            return validationConfig(label: validateOldPasswordLabel, state: false)
        }
        
        viewModel.validateCurrentPassword(email: email, password: password) { [weak self] error in
            if error != nil {
                self?.validateOldPasswordLabel.text = "The current password is not related to your email."
            }
        }
        allPropertiesAreValid()
        return validationConfig(label: validateOldPasswordLabel, state: true)
    }
    
    @objc private func validateNewPassword() -> Bool {
        guard let password = newPasswordTextField.text else { return false }
        guard validator.validatePassword(password: password) else {
            configureSaveBtn(with: 0.5, state: false)
            return validationConfig(label: validateNewPasswordLabel, state: false)
        }
        allPropertiesAreValid()
        return validationConfig(label: validateNewPasswordLabel, state: true)
    }
    
    @objc private func validateRepeatPassword() -> Bool {
        guard
            let currentPass = newPasswordTextField.text,
            let newPass = repeatNewPasswordTextField.text
        else { return false }
        
        guard currentPass == newPass else {
            configureSaveBtn(with: 0.5, state: false)
            return validationConfig(label: validateRepeatNewPasswordLabel, state: false)
        }
        allPropertiesAreValid()
        return validationConfig(label: validateRepeatNewPasswordLabel, state: true)
    }
    
    func validationConfig(label: UILabel, state: Bool) -> Bool {
        label.isHidden = state
        return state
    }
    
    func allPropertiesAreValid() {
        guard
            let email = emailTextField.text,
            let oldPassword = oldPasswordTextField.text,
            let newPassword = newPasswordTextField.text,
            let repeatPassword = repeatNewPasswordTextField.text
        else { return }
        
        let emailValidation = validator.validateEmail(email: email)
        let oldPasswordValidation = validator.validatePassword(password: oldPassword)
        let newPasswordValidation = validator.validatePassword(password: newPassword)
        let repeatPasswordValidation = newPassword == repeatPassword
        
        if emailValidation && oldPasswordValidation && newPasswordValidation && repeatPasswordValidation {
            configureSaveBtn(with: 1, state: true)
        }
    }
}

// MARK: - Delegates
extension ProfileViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        emailTextField.endEditing(true)
        oldPasswordTextField.endEditing(true)
        newPasswordTextField.endEditing(true)
        repeatNewPasswordTextField.endEditing(true)
        return false
    }
}
