//
//  CustomTabBarController.swift
//  news-app
//
//  Created by Stefan Aleksiev on 2.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    // MARK: - Coordinator
    var coordinator: TabBarCoordinator
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        ActivityIndicatorManager.shared.start(view: self.view)
        setup()
    }
    
    // MARK: - Init
    init(coordinator: TabBarCoordinator) {
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Setup
private extension TabBarController {
    func setup() {
        tabBar.barTintColor = .white

        coordinator.newsPaperNavigation.tabBarItem = itemFactory(title: Constants.Titles.personalNewspaper, imagePath: Constants.TabBar.Images.newsPaper, selectedImagePath: Constants.TabBar.Images.newsPaperSelected)
        
        coordinator.worldNewsNavigation.tabBarItem = itemFactory(title: Constants.Titles.worldNews, imagePath: Constants.TabBar.Images.worldNews, selectedImagePath: Constants.TabBar.Images.worldNewsSelected)
        
        coordinator.favouritesNavigation.tabBarItem = itemFactory(title: Constants.Titles.favourite, imagePath: Constants.TabBar.Images.favourites, selectedImagePath: Constants.TabBar.Images.favouritesSelected)
        
        viewControllers = [coordinator.newsPaperNavigation, coordinator.worldNewsNavigation, coordinator.favouritesNavigation]
        self.selectedIndex = 1
    }
}

// MARK: - Delegates
extension TabBarController: UITabBarControllerDelegate {
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.title {
        case Titles.Newspaper.rawValue:
            if !coordinator.newsPaperStarted {
                ActivityIndicatorManager.shared.start(view: self.view)
                coordinator.startNewsPaper()
            }
        case Titles.SavedNews.rawValue:
            if !coordinator.favouritesStarted {
                ActivityIndicatorManager.shared.start(view: self.view)
                coordinator.startFavourites()
            }
        default:
            break
        }
    }
}

// MARK: - Additional Funtions
private extension TabBarController {
    func itemFactory(title: String, imagePath: String, selectedImagePath: String) -> UITabBarItem {
        let item = UITabBarItem(title: title, image: UIImage(named: imagePath), selectedImage: UIImage(named: selectedImagePath))
        return item
    }
}

enum Titles: String {
    case Newspaper = "Personal Newspaper"
    case WorldNews = "World News"
    case SavedNews = "Saved News"
}
