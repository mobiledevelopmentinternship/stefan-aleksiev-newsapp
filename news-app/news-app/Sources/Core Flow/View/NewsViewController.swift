//
//  WorldNewsViewController.swift
//  news-app
//
//  Created by Stefan Aleksiev on 4.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit
import Reachability
import Popover
import RealmSwift

class NewsViewController: UIViewController {
    
    // MARK: - Coordinator
    var coordinator: NewsCoordinator
    
    // MARK: - IBOutlets
    @IBOutlet private weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var categoryCollectionView: UICollectionView!
    @IBOutlet private weak var articleCollectionView: UICollectionView!
    @IBOutlet private weak var rechabilityLabel: UILabel!
    
    // MARK: - Popover
    private var popover: Popover!
    private lazy var popoverOptions: [PopoverOption] = [
        .type(.auto),
        .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))
    ]
    
    // MARK: - Properties
    private var presenter: NewsPresenter!
    private let factory = Factory()
    private var isWorldNews = true
    private let viewModel: ArticleViewModel
    private var selectedCategory = CategoryCollectionViewCell()
    private var selectedCountryCell = CountryTableViewCell()
    private let repository = RealmManager(repository: RealmClient())
    private let offlineArticles = [Article]()
    
    private let categories: [String] = {
        return Category.getCategories()
    }()
    private let countries: [String] = {
        return Country.getCountries()
    }()
    
    // MARK: - Init
    init(coordinator: NewsCoordinator, viewModel: ArticleViewModel) {
        self.coordinator = coordinator
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        articleCollectionView.reloadData()
    }
    
    func isWorldNews(state: Bool) {
        isWorldNews = state
    }
}

// MARK: - Actions
extension NewsViewController: ReadButtonDelegate {
    func readBtnPressed(_ article: Article) {
        coordinator.startNewsDetails(articleModel: article)
    }
    
    @objc func didNavigate() {
        coordinator.startProfile()
    }
    
    @objc func onDidReceiveStatus(_ notification: Notification) {
        if Connectivity.shared.isConnected {
            if !isWorldNews {
                showCategories()
                fetchPersonalHeadlinesFromAPI()
            } else {
                hideCategories()
                fetchTopHeadlinesFromAPI()
            }
        }
        setRechabilityLabel()
        setup()
        categoryCollectionView.reloadData()
    }
}

// MARK: - Setup
private extension NewsViewController {
    func setup() {
        presenter = NewsPresenter(collectionViewDataSource: self, collectionViewDelegate: self, tableViewDataSource: self, tableViewDelegate: self)
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveStatus(_:)), name: .didReceiveStatus, object: nil)
        rechabilityLabel.isHidden = true
        
        // MARK: - Fetch Offline Articles from DB if there is no connection
        if !Connectivity.shared.isConnected {
            !isWorldNews ? viewModel.fetchPersonalHeadlinesFromDB() : viewModel.fetchTopHeadlinesFromDB()
        }
        
        guard viewModel.articles.list.isEmpty else {
            setupCollectionViews()
            ActivityIndicatorManager.shared.stop()
            return
        }
        
        setRechabilityLabel()
        
        ActivityIndicatorManager.shared.stop()
    }
    
    func setupCollectionViews() {
        presenter.registerCell(categoryCollectionView, cellType: CategoryCollectionViewCell.self)
        presenter.registerCell(articleCollectionView, cellType: ArticleCollectionViewCell.self)
        presenter.setupCollectionView(in: self, collectionView: articleCollectionView, anotherCollectionView: categoryCollectionView)
        
        !isWorldNews ? showCategories() : hideCategories()
    }
    
    func showCategories() {
        setButtons()
        factory.addBorders(categoryCollectionView)
        collectionViewHeightConstraint.constant = 30
    }
    
    func hideCategories() {
        let statusIndicator = updateStatusIndicator(with: Connectivity.shared.isConnected)
        let profile = factory.setNavigationButtonItem(self, action: #selector (didNavigate), iconPath: Constants.NavigationItem.Profile.icon)
        navigationItem.rightBarButtonItems = [profile, statusIndicator]
        navigationItem.leftBarButtonItems = AppManager.shared.setDateNavigationItem()
        collectionViewHeightConstraint.constant = 0
    }
    
    func updateStatusIndicator(with status: Bool) -> UIBarButtonItem {
        return factory.addIndicatorButtonItem(status: status)
    }
}

// MARK: - Additional Functions
private extension NewsViewController {
    func setButtons() {
        let selectedCategory = AppManager.shared.selectedCountry
        let statusIndicator = updateStatusIndicator(with: Connectivity.shared.isConnected)
        let countryButton = factory.setNavigationButtonItem(self, action: #selector (openCountries), iconPath: "\(selectedCategory.rawValue).png")
        
        if !Connectivity.shared.isConnected {
            countryButton.isEnabled = false
        } else { countryButton.isEnabled = true }
        
        navigationItem.rightBarButtonItems = [countryButton, statusIndicator]
    }
    
    func setRechabilityLabel() {
        if !Connectivity.shared.isConnected && viewModel.articles.list.isEmpty {
            rechabilityLabel.text = Constants.Labels.unavailableConnection
            rechabilityLabel.center = self.view.center
            rechabilityLabel.font = Font.bebasBold.of(size: .large)
            rechabilityLabel.textColor = Colors.secondComplimentary
            rechabilityLabel.isHidden = false
        }
    }
}

// MARK: - Popover Menu
extension NewsViewController: UITableViewDelegate, UITableViewDataSource {
    @objc func openCountries() {
        let tableView = UITableView(frame: CGRect(x: self.view.frame.width - 40, y: 70, width: 50, height: 320))
        
        let startPoint = CGPoint(x: self.view.frame.width - 40, y: 70)
        popover = Popover(options: popoverOptions, showHandler: nil, dismissHandler: nil)
        
        presenter.setupTableView(in: self, tableView: tableView)
        presenter.registerCell(tableView, cellType: CountryTableViewCell.self)
        
        tableView.isScrollEnabled = false
        tableView.separatorStyle = .none
        
        self.popover = Popover(options: self.popoverOptions)
        popover.show(tableView, point: startPoint)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        articleCollectionView.contentInset = Constants.CollectionView.contentInset
        
        guard let selectedCountry = Country(rawValue: countries[indexPath.row]) else { return }
        AppManager.shared.setCountry(selectedCountry)
        selectedCountryCell = tableView.cellForRow(at: indexPath) as! CountryTableViewCell
        
        coordinator.fetchPersonalHeadlines { [weak self] _ in
            self?.postConfigure(indexPath)
            self?.reloadData()
        }
        setButtons()
        self.popover.dismiss()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let countryCell = tableView.dequeueReusableCell(withIdentifier: CountryTableViewCell.reuseIdentifier) as? CountryTableViewCell else { return UITableViewCell() }
        countryCell.configure(with: countries[indexPath.row])
        return countryCell
    }
}

// MARK: - Delegates DataSource
extension NewsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case articleCollectionView:
            return viewModel.articles.list.count
        default:
            return categories.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case articleCollectionView:
            guard let cell = articleCollectionView.dequeueReusableCell(withReuseIdentifier: ArticleCollectionViewCell.reuseIdentifier, for: indexPath) as? ArticleCollectionViewCell else { return UICollectionViewCell()}
            cell.configure(with: viewModel.articles.list[indexPath.row])
            cell.delegate = self
            return cell
        default:
            guard let cell = categoryCollectionView.dequeueReusableCell(withReuseIdentifier: CategoryCollectionViewCell.reuseIdentifier, for: indexPath) as? CategoryCollectionViewCell else { return UICollectionViewCell()}
            
            if !Connectivity.shared.isConnected {
                cell.isUserInteractionEnabled = false
                cell.setDisabled(with: categories[indexPath.row])
            } else {
                cell.isUserInteractionEnabled = true
                cell.configure(with: categories[indexPath.row]) }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        articleCollectionView.contentInset = Constants.CollectionView.contentInset
        
        if collectionView == categoryCollectionView {
            selectedCategory = collectionView.cellForItem(at: indexPath) as! CategoryCollectionViewCell
            guard let defaultCell = collectionView.cellForItem(at: IndexPath(item: 2, section: 0)) as? CategoryCollectionViewCell else { return }
            
            selectedCategory.didSelect(defaultCell: defaultCell, selectedCell: selectedCategory)
            
            if selectedCategory.isSelected {
                coordinator.fetchPersonalHeadlines { [weak self] _ in
                    self?.postConfigure(indexPath)
                    self?.reloadData()
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? CategoryCollectionViewCell
        cell?.didDeselect()
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let currentCell = cell as? CategoryCollectionViewCell
        currentCell?.didDeselect()
    }
    
    func reloadData() {
        ActivityIndicatorManager.shared.start(view: self.view)
        self.articleCollectionView.reloadData()
        ActivityIndicatorManager.shared.stop()
    }
}

// MARK: - Delegates Style
extension NewsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == articleCollectionView {
            return CGSize(width: self.view.frame.width / 1.4 , height: articleCollectionView.bounds.height)
        }
        return CGSize(width: categories[indexPath.row].size(withAttributes: nil).width * Constants.Border.widthMultipler, height: categoryCollectionView.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == categoryCollectionView {
            let currentCell = cell as? CategoryCollectionViewCell
            currentCell?.didDeselect()
            
            if currentCell?.getCategoryName() == Category.General.rawValue {
                currentCell?.configure(with: Category.General.rawValue)
            }
        } else {
            cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            UIView.animate(withDuration: 0.5) { cell.transform = CGAffineTransform.identity }
        }
        
        if selectedCategory.isSelected {
            selectedCategory.didSelect(selectedCell: selectedCategory)
        }
    }
    
    func postConfigure(_ indexPath: IndexPath) {
        let indexPath = IndexPath(row: 0, section: 0)
        articleCollectionView.scrollToItem(at: indexPath, at: .left, animated: false)
    }
}

// MARK: - Post fetch
private extension NewsViewController {
    func fetchTopHeadlinesFromAPI() {
        viewModel.requestTopHeadlines { result in
            
            switch result {
            case .success(let data):
                self.viewModel.articles = data
                self.postSetup()
            case .failure(let error):
                self.showAlert(title: Constants.Error.title, message: error.localizedDescription)
            }
        }
    }
    
    func fetchPersonalHeadlinesFromAPI() {
        viewModel.requestPersonalHeadlines { result in
            switch result {
            case .success(let data):
                self.viewModel.articles = data
                self.postSetup()
            case .failure(let error):
                self.showAlert(title: Constants.Error.title, message: error.localizedDescription)
            }
        }
    }

    func postSetup() {
        presenter.registerCell(categoryCollectionView, cellType: CategoryCollectionViewCell.self)
        presenter.registerCell(articleCollectionView, cellType: ArticleCollectionViewCell.self)
        presenter.setupCollectionView(in: self, collectionView: articleCollectionView, anotherCollectionView: categoryCollectionView)
    }
}
