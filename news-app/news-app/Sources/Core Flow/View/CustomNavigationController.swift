//
//  CustomNavigationController.swift
//  news-app
//
//  Created by Stefan Aleksiev on 18.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
}

private extension CustomNavigationController {
    
    // MARK: - Setup
    func setup() {
        navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationBar.layoutMargins.left = Margin.left.rawValue
        navigationBar.prefersLargeTitles = true
        navigationBar.setValue(true, forKey: "hidesShadow")
        
        let largeTitleAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: Font.bebasBold.of(size: .extraLarge)
        ]
        
        let smallTitleAttributes = [
            NSAttributedString.Key.foregroundColor : UIColor.white,
            NSAttributedString.Key.font: Font.bebasBold.of(size: .semiLarge)
        ]
        
        navigationBar.titleTextAttributes = smallTitleAttributes
        navigationBar.largeTitleTextAttributes = largeTitleAttributes
    }
}
