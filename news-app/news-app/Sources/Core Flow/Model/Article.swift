//
//  News.swift
//  news-app
//
//  Created by Stefan Aleksiev on 10.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import RealmSwift

class Article: Object, Codable {
    
    @objc dynamic var author: String?
    @objc dynamic var source: Source?
    @objc dynamic var title: String?
    @objc dynamic var desc: String?
    @objc dynamic var content: String?
    @objc dynamic var url: String?
    @objc dynamic var urlToImage: String?
    @objc dynamic var publishedAt: String?
    @objc dynamic var isSaved: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case author = "author"
        case source = "source"
        case title = "title"
        case desc = "desc"
        case content = "content"
        case url = "url"
        case urlToImage = "urlToImage"
        case publishedAt = "publishedAt"
    }

    override static func primaryKey() -> String? {
        return "url"
    }
}


