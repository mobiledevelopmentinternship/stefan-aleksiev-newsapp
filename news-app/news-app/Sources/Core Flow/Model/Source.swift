//
//  Source.swift
//  news-app
//
//  Created by Stefan Aleksiev on 6.08.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import RealmSwift

class Source: Object, Codable {
    
    @objc dynamic var id: String?
    @objc dynamic var name: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
    }
}
