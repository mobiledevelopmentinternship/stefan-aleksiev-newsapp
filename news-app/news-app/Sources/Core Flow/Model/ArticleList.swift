//
//  ArticleList.swift
//  news-app
//
//  Created by Stefan Aleksiev on 6.08.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import RealmSwift

class ArticleList: Object, Codable {
    
    var list = [Article]()
    var articleList = List<Article>()
    @objc dynamic var primaryKey: String?
    @objc dynamic var category: String?
    @objc dynamic var country: String?
    
    enum CodingKeys: String, CodingKey {
        case list = "articles"
    }
    
    override static func primaryKey() -> String? {
        return "primaryKey"
    }
}
