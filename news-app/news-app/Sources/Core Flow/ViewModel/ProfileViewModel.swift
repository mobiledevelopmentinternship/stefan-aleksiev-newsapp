//
//  SettingsViewModel.swift
//  news-app
//
//  Created by Stefan Aleksiev on 16.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import Firebase

class ProfileViewModel {
    
    // MARK: - Properties
    
    // MARK: - Init
    init() {}
}

// MARK: - Additional Functions
extension ProfileViewModel {
    func singOut(errorHandler: BaseErrorHandler) {
        
        do {
            try Auth.auth().signOut()
            AppManager.shared.isLoggedIn = false
        }
        catch let error { errorHandler(error) }
    }
    
    func updateUser(errorHandler: @escaping BaseErrorHandler, newEmail: String, password: String) {
        guard let currentUser = Auth.auth().currentUser else { return }
        
        currentUser.updateEmail(to: newEmail, completion: { (error) in
            guard error == nil else {
                guard let err = error else { return }
                errorHandler(err)
                return
            }
        })
        
        currentUser.updatePassword(to: password, completion: { (error) in
            guard error == nil else {
                guard let err = error else { return }
                errorHandler(err)
                return
            }
        })
    }
    
    func getUserEmail() -> String {
        guard let currentUser = Auth.auth().currentUser else { return "" }
        guard let email = currentUser.email else { return "" }
        return email
    }
    
    func validateCurrentPassword(email: String, password: String, errorHandler: @escaping BaseErrorHandler) {
        let credential = EmailAuthProvider.credential(withEmail: email, password: password)
        Auth.auth().currentUser?.reauthenticate(with: credential, completion: { (_, error) in
            guard error == nil else {
                guard let err = error else { return }
                errorHandler(err)
                return
            }
        })
    }
}
