//
//  ArticleDetailsViewModel.swift
//  news-app
//
//  Created by Stefan Aleksiev on 24.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

class ArticleDetailsViewModel {
    
    // MARK: - Properties
    private let repository = RealmManager(repository: RealmClient())
    var article: Article
    
    // MARK: - Init
    init(article: Article) {
        self.article = article
    }
}

// MARK: - Additional Functions
extension ArticleDetailsViewModel {
    func isSaved(_ article: Article) -> Bool {
        return repository.isSavedInDB(with: article)
    }
}
