//
//  FavouritesViewModel.swift
//  news-app
//
//  Created by Stefan Aleksiev on 16.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

class FavouritesViewModel {
    
    // MARK: - Properties
    var articles: [Article]
    private let repository = RealmManager(repository: RealmClient())
    
    // MARK: - Init
    init(articles: [Article]) {
        self.articles = articles
    }
}

// MARK: - Additional Functions
extension FavouritesViewModel {
    func fetchArticles() -> [Article] {
        return repository.fetchArticlesFromDB()
    }
}
