//
//  NewsViewModel.swift
//  news-app
//
//  Created by Stefan Aleksiev on 10.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

typealias CompletionHandler = (CustomResultType<ArticleList, Error>) -> ()

class ArticleViewModel {
    
    // MARK: - Properties
    var articles: ArticleList
    private let service = APIClient()
    private let repository = RealmManager(repository: RealmClient())
    
    // MARK: - Init
    init(articles: ArticleList) {
        self.articles = articles
    }
}

// MARK: - Requests
extension ArticleViewModel {
    func requestTopHeadlines(completionHandler: @escaping CompletionHandler) {
        service.topHeadlines(country: Country.us) { [weak self] result in
            switch result {
            case .success(let data):
                let jsonData = try? JSONDecoder().decode(ArticleList.self, from: data)
                guard let articles = jsonData else {
                    completionHandler(.success(ArticleList()))
                    return
                }
                
                self?.repository.saveOfflineArticles(errorHandler: { error in
                    completionHandler(.failure(error))
                }, articles: articles, PK: Country.us.rawValue, country: Country.us.rawValue)
                
                completionHandler(.success(articles))
            case .failure(let error):
                completionHandler(.failure(error))
            }
        }
    }
    
    func requestPersonalHeadlines(completionHandler: @escaping CompletionHandler) {
        let selectedCategory = AppManager.shared.selectedCategory
        let selectedCountry = AppManager.shared.selectedCountry
        
        service.personalHeadlines(category: selectedCategory, country: selectedCountry) { [weak self] result in
            switch result {
            case .success(let data):
                let jsonData = try? JSONDecoder().decode(ArticleList.self, from: data)
                guard let articles = jsonData else {
                    completionHandler(.success(ArticleList()))
                    return
                }
                
                self?.repository.saveOfflineArticles(errorHandler: { error in
                    completionHandler(.failure(error))
                }, articles: articles,
                   PK: selectedCountry.rawValue + selectedCategory.rawValue,
                   category: selectedCategory.rawValue,
                   country: selectedCountry.rawValue)
                
                completionHandler(.success(articles))
            case .failure(let error):
                completionHandler(.failure(error))
            }
        }
    }
}

// MARK: - Fetch Offline Data
extension ArticleViewModel {
    func fetchTopHeadlinesFromDB() {
        let offlineArticle = repository.fetchOfflineArticlesFromDB(primaryKey: Country.us.rawValue)
        var mappedArticles = [Article]()
        
        if offlineArticle.count != 0 {
            for i in offlineArticle[0].articleList { mappedArticles.append(i) }
        }
        articles.list = mappedArticles
    }
    
    
    func fetchPersonalHeadlinesFromDB() {
        let selectedCategory = AppManager.shared.selectedCategory
        let selectedCountry = AppManager.shared.selectedCountry
        
        let offlineArticle = repository.fetchOfflineArticlesFromDB(primaryKey: selectedCountry.rawValue + selectedCategory.rawValue)
        var mappedArticles = [Article]()
        
        if offlineArticle.count != 0 {
            for i in offlineArticle[0].articleList { mappedArticles.append(i) }
        }
        articles.list = mappedArticles
    }
}

// MARK: - Result Type
enum CustomResultType<ArticleList, Error> {
    case success(ArticleList)
    case failure(Error)
}
