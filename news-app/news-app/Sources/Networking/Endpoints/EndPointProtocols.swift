//
//  EndPointProtocols.swift
//  news-app
//
//  Created by Stefan Aleksiev on 10.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

protocol EndPoints {
    func topHeadlines(country: Country, completionHandler: @escaping (Result<Data>) -> ())
    func personalHeadlines(category: Category, country: Country, completionHandler: @escaping (Result<Data>) -> ())
}
