//
//  APIRouter.swift
//  news-app
//
//  Created by Stefan Aleksiev on 10.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import Alamofire

enum APIRouter {
    case topHeadlines(country: Country)
    case personalHeadlines(category: Category, country: Country)
    
    static let baseURL = "https://newsapi.org/v2"
    static let apiKey = "9dc1f8d385184b2a802f242dd71f90a4"
    
    private var path: String {
        switch self {
        case .topHeadlines:
            return "/top-headlines/"
        case .personalHeadlines:
            return "/top-headlines/"
        }
    }
    
    private var method: HTTPMethod {
        switch self {
        case .topHeadlines, .personalHeadlines:
            return .get
        }
    }
    
    private var parameters: Parameters? {
        switch self {
        case .topHeadlines(let country):
            return ["country": country, "apiKey": APIRouter.apiKey]
        case .personalHeadlines(let category, let country):
            return ["category": category, "country": country, "apiKey": APIRouter.apiKey]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        guard let url = URL(string: APIRouter.baseURL) else { throw URlError.invalid }
        let wholeURL = url.appendingPathComponent(path)
        guard let encodedURL = URL(string: wholeURL.absoluteString.removingPercentEncoding!) else { throw URlError.invalid }
        
        var request = URLRequest(url: encodedURL)
        request.httpMethod = method.rawValue
        
        if let params = parameters {
            let queryRequest = try URLEncoding.default.encode(request, with: params)
            return queryRequest
        }
        else {
            var request = URLRequest(url: encodedURL)
            request.httpMethod = method.rawValue
            return request
        }
    }
}

enum URlError: Error {
    case invalid
}

enum Result<Value> {
    case success(Value)
    case failure(Error)
}

enum Category: String, RawRepresentable, CaseIterable {
    case Business
    case Entertainment
    case General
    case Health
    case Science
    case Sports
    case Technology
    
    static func getCategories() -> [String] {
         return Category.allCases.map { $0.rawValue }
    }
}

enum Country: String, RawRepresentable, CaseIterable {
    case mx
    case lv
    case ua
    case co
    case eg
    case ru
    case ro
    case cz
    case us
    case bg
    
    static func getCountries() -> [String] {
        return Country.allCases.map { $0.rawValue }
    }
}


