//
//  APIService.swift
//  news-app
//
//  Created by Stefan Aleksiev on 10.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import Alamofire

typealias BaseCompletionHandler = ((Result<Data>) -> ())
typealias BaseErrorHandler = (Error) -> ()

class APIClient: EndPoints {
    func topHeadlines(country: Country, completionHandler: @escaping BaseCompletionHandler) {
        guard let url = try? APIRouter.topHeadlines(country: country).asURLRequest() else { return }
        
        AF.request(url).responseDecodable { (response: DataResponse<ArticleList>) in
            if let error = response.error {
                completionHandler(.failure(error))
                return
            }
            
            guard let data = response.data else { return }
            completionHandler(.success(data))
        }
    }
    
    func personalHeadlines(category: Category, country: Country, completionHandler: @escaping (Result<Data>) -> ()) {
        guard let url = try? APIRouter.personalHeadlines(category: category, country: country).asURLRequest() else { return }
        
        AF.request(url).responseDecodable { (response: DataResponse<ArticleList>) in
            if let error = response.error {
                completionHandler(.failure(error))
                return
            }
            
            guard let data = response.data else { return }
            completionHandler(.success(data))
        }
    }
}
