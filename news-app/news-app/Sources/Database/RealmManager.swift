//
//  RealmManager.swift
//  news-app
//
//  Created by Stefan Aleksiev on 22.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import RealmSwift

class RealmManager {
    
    private let repository: RepositoryProtocol
    
    // MARK: - Init
    init(repository: RepositoryProtocol) {
        self.repository = repository
    }
    
    func saveArticleToDB(errorHandler: BaseErrorHandler, model: Article) {
        repository.saveArticleToDB(errorHandler: errorHandler, model)
    }
    
    func saveOfflineArticles(errorHandler: BaseErrorHandler,
                             articles: ArticleList,
                             PK: String,
                             category: String? = nil,
                             country: String? = nil)
    {
        repository.saveOfflineArticles(errorHandler: errorHandler, articles, with: PK, category: category, country: country)
    }
    
    func fetchArticlesFromDB() -> [Article] {
        return repository.fetchArticlesFromDB()
    }
    
    func fetchOfflineArticlesFromDB(primaryKey: String) -> [ArticleList] {
        return repository.fetchOfflineArticlesFromDB(primaryKey: primaryKey)
    }
    
    func removeArticleFromDB(errorHandler: BaseErrorHandler, model: Article) {
        repository.removeArticleFromDB(errorHandler: errorHandler, model)
    }
    
    func isSavedInDB(with model: Article) -> Bool {
        return repository.isSavedInDB(model)
    }
}
