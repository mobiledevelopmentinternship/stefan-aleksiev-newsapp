//
//  RepositoryProtocol.swift
//  news-app
//
//  Created by Stefan Aleksiev on 26.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

protocol RepositoryProtocol {
    
    func saveArticleToDB(errorHandler: BaseErrorHandler, _ article: Article)
    func saveOfflineArticles(errorHandler: BaseErrorHandler, _ articles: ArticleList, with PK: String, category: String?, country: String?)
    
    func fetchArticlesFromDB() -> [Article]
    func fetchOfflineArticlesFromDB(primaryKey: String) -> [ArticleList]
    
    func removeArticleFromDB(errorHandler: BaseErrorHandler, _ article: Article)
    func isSavedInDB(_ article: Article) -> Bool
}
