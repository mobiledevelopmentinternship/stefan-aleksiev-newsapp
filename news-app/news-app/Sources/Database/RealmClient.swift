//
//  RealmClient.swift
//  news-app
//
//  Created by Stefan Aleksiev on 26.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import RealmSwift

class RealmClient: RepositoryProtocol {
    
    private let realm = try! Realm()
    
    func saveArticleToDB(errorHandler: BaseErrorHandler, _ article : Article) {
        
        do {
            try realm.write {
                article.isSaved = true
                realm.create(Article.self, value: article, update: .all)
            }
            
        } catch let error {
            errorHandler(error)
        }
    }
    
    func saveOfflineArticles(
        errorHandler: BaseErrorHandler,
        _ articles: ArticleList,
        with PK: String,
        category: String?,
        country: String?)
    {

        let object = ArticleList()
        object.primaryKey = PK
        
        _ = articles.list.map({ article -> Article in
            object.articleList.append(article)
            return article
        })
        
        object.category = category ?? nil
        object.country = country ?? nil

        do {
            try realm.write {
                for i in articles.list { if isSavedInDB(i) { return } else { self.realm.create(ArticleList.self, value: object, update: .all) }}
            }
        } catch let error {
            errorHandler(error)
        }
    }
    
    func fetchArticlesFromDB() -> [Article] {
        let articles = realm.objects(Article.self).filter("isSaved == true")
        return Array(articles)
    }
    
    func fetchOfflineArticlesFromDB(primaryKey: String) -> [ArticleList] {
        let offlineArticles = realm.objects(ArticleList.self).filter("primaryKey == %@", primaryKey)
        return Array(offlineArticles)
    }
    
    func removeArticleFromDB(errorHandler: BaseErrorHandler, _ article: Article) {

        do {
            try realm.write {
                if let articleToDelete = realm.object(ofType: Article.self, forPrimaryKey: article.url) {
                    realm.delete(articleToDelete)
                }
            }
        } catch let error {
            errorHandler(error)
        }
    }
    
    func isSavedInDB(_ article: Article) -> Bool {
        guard let isSaved = realm.object(ofType: Article.self, forPrimaryKey: article.url)?.isSaved else { return false }
        return isSaved
    }
}
