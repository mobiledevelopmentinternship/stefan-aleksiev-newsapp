//
//  NewsPaperCoordinator.swift
//  news-app
//
//  Created by Stefan Aleksiev on 3.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class NewsPaperCoordinator: Coordinator {

    // MARK: - Properties
    var navigationController: UINavigationController
    var childCoordinators = [Coordinator]()
    
    // MARK: - Init
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        AppThemeConfigurator.shared.setupNavigation(with: navigationController)
    }
    
    func start() {
        startNewsPaperCoordinator()
    }
}

// MARK: - Prepare & Start
extension NewsPaperCoordinator {
    func startNewsPaperCoordinator() {
        let newsPaperViewController = NewsPaperViewController(nibName: nil, bundle: nil)
        newsPaperViewController.coordinator = self
        navigationController.pushViewController(newsPaperViewController, animated: true)
    }
    
    func startSettingsCoordinator() {
        let settingsCoordinator = ProfileCoordinator(navigationController: navigationController)
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        settingsCoordinator.delegate = self
        childCoordinators.append(settingsCoordinator)
        settingsCoordinator.start()
    }
}

// MARK: - Delegates
extension NewsPaperCoordinator: BackwardsNavigable {
    func navigateBack(to: Coordinator) {
        navigationController.popToRootViewController(animated: true)
        childCoordinators.removeLast()
    }
}
