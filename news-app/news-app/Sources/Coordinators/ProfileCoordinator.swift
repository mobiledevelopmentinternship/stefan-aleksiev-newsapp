//
//  SettingsCoordinator.swift
//  news-app
//
//  Created by Stefan Aleksiev on 5.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class ProfileCoordinator: Coordinator {
    
    // MARK: - Delegates
    weak var delegate: BackwardsNavigable?
    
    // MARK: - Properties
    var navigationController: CustomNavigationController
    var childCoordinators = [Coordinator]()
    
    // MARK: - Additional Properties
    private var viewModel = ProfileViewModel()
    
    // MARK: - Init
    required init(navigationController: CustomNavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        startProfileCoordinator()
    }
}

// MARK: - Prepare & Start
extension ProfileCoordinator {
    private func startProfileCoordinator() {
        let profileViewController = ProfileViewController(coordinator: self, viewModel: viewModel)
        profileViewController.coordinator = self
        navigationController.pushViewController(profileViewController, animated: true)
    }
    
    func startLoginCoordinator() {
        navigationController.viewControllers.removeAll()
        
        let tabBarController = AppDelegate.shared.rootViewController as? TabBarController
        tabBarController?.tabBar.isHidden = true
        
        let model = User()
        let viewModel = LoginViewModel(user: model)
        let loginViewController = LoginViewController(coordinator: LoginCoordinator(navigationController: navigationController), viewModel: viewModel)
        loginViewController.coordinator = LoginCoordinator(navigationController: navigationController)
        navigationController.pushViewController(loginViewController, animated: true)
        AppDelegate.shared.setRootViewController(with: loginViewController)
    }
}

// MARK: - Delegates
extension ProfileCoordinator {
    func didNavigateBack() {
        delegate?.navigateBack(to: self)
    }
}
