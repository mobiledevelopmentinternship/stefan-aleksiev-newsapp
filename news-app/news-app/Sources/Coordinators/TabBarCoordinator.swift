//
//  MainTabBarCoordinator.swift
//  news-app
//
//  Created by Stefan Aleksiev on 3.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class TabBarCoordinator: Coordinator {

    // MARK: - Properties
    var navigationController: CustomNavigationController
    var worldNewsStarted = false
    var newsPaperStarted = false
    var favouritesStarted = false
    
    // MARK: - Flows
    var worldNewsNavigation = CustomNavigationController()
    var newsPaperNavigation = CustomNavigationController()
    var favouritesNavigation = CustomNavigationController()
    
    var childCoordinators = [Coordinator]()
    
    // MARK: - Init
    init(navigationController: CustomNavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        startWorldNews()
    }
}

// MARK: - Start Child ViewController
extension TabBarCoordinator: InitiateCoordinators {
    func startWorldNews() {
        let rootVC = TabBarController(coordinator: self)
        
        AppDelegate.shared.setRootViewController(with: rootVC)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = rootVC
        
        worldNewsStarted = true
        let worldNewsCoordinator = NewsCoordinator(navigationController: worldNewsNavigation)
        childCoordinators.append(worldNewsCoordinator)
        worldNewsCoordinator.start()
    }
    
    func startNewsPaper() {
        newsPaperStarted = true
        let newsPaperCoordinator = NewsCoordinator(navigationController: newsPaperNavigation)
        childCoordinators.append(newsPaperCoordinator)
        newsPaperCoordinator.startNewsPaper()
    }
    
    func startFavourites() {
        favouritesStarted = true
        let favouritesCoordinator = FavouritesCoordinator(navigationController: favouritesNavigation)
        childCoordinators.append(favouritesCoordinator)
        favouritesCoordinator.start()
    }
}
