//
//  SettingsCoordinator.swift
//  news-app
//
//  Created by Stefan Aleksiev on 5.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class SettingsCoordinator: Coordinator {
    
    // MARK: - Delegates
    weak var delegate: BackwardsNavigable?
    
    // MARK: - Properties
    var navigationController: CustomNavigationController
    var childCoordinators = [Coordinator]()
    
    // MARK: - Additional Properties
    private var viewModel = SettingsViewModel()
    
    // MARK: - Init
    required init(navigationController: CustomNavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        startSettingsCoordinator()
    }
}

// MARK: - Prepare & Start
private extension SettingsCoordinator {
    func startSettingsCoordinator() {
        let settingsViewController = SettingsViewController(coordinator: self, viewModel: viewModel)
        settingsViewController.coordinator = self
        navigationController.pushViewController(settingsViewController, animated: true)
    }
}

extension SettingsCoordinator {
    func didNavigateBack() {
        delegate?.navigateBack(to: self)
    }
}
