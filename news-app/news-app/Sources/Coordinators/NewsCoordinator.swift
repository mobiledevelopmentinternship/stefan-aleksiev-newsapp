//
//  WorldNewsCoordinator.swift
//  news-app
//
//  Created by Stefan Aleksiev on 4.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class NewsCoordinator: Coordinator {
    
    // MARK: - Properties
    var navigationController: CustomNavigationController
    var childCoordinators = [Coordinator]()
    
    // MARK: - Aditional Properties
    private let viewModel = ArticleViewModel(articles: ArticleList())
    
    // MARK: - Init
    init(navigationController: CustomNavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        startWorldNews()
    }
}

// MARK: - Prepare & Start
extension NewsCoordinator {
    func startWorldNews() {
        fetchTopHeadlines { [weak self] _ in
            self?.configure(title: Constants.Titles.worldNews)
        }
    }
    
    func startNewsPaper() {
        fetchPersonalHeadlines { [weak self] _ in
            self?.configure(title: Constants.Titles.personalNewspaper, state: false)
        }
    }
    
    func startNewsDetails(articleModel: Article) {
        let newsDetailsCoordinator = NewsDetailsCoordinator(navigationController: navigationController)
        newsDetailsCoordinator.delegate = self
        newsDetailsCoordinator.viewModel.article = articleModel
        childCoordinators.append(newsDetailsCoordinator)
        newsDetailsCoordinator.start()
    }
    
    func startProfile() {
        let profileCoordinator = ProfileCoordinator(navigationController: navigationController)
        profileCoordinator.delegate = self
        childCoordinators.append(profileCoordinator)
        profileCoordinator.start()
    }
    
    func configure(title: String, state: Bool? = true) {
        let viewController = NewsViewController(coordinator: self, viewModel: self.viewModel)
        viewController.title = title
        viewController.coordinator = self
        viewController.isWorldNews(state: state ?? true)
        navigationController.pushViewController(viewController, animated: true)
    }
}

// MARK: - Delegates
extension NewsCoordinator: BackwardsNavigable {
    func navigateBack(to: Coordinator) {
        navigationController.popToRootViewController(animated: true)
        childCoordinators.removeLast()
    }
}

// MARK: - Request
extension NewsCoordinator {
    func fetchTopHeadlines(completionHandler: @escaping CompletionHandler) {
        if !Connectivity.shared.isConnected { completionHandler(.success(ArticleList())) } else {
            viewModel.requestTopHeadlines { [weak self] result in
                switch result {
                case .success(let articles):
                    self?.viewModel.articles = articles
                    completionHandler(.success(articles))
                case .failure(let error):
                    completionHandler(.failure(error))
                }
            }
        }
    }
    
    func fetchPersonalHeadlines(completionHandler: @escaping CompletionHandler) {
        if !Connectivity.shared.isConnected { completionHandler(.success(ArticleList())) } else {
        viewModel.requestPersonalHeadlines { [weak self] result in
            switch result {
            case .success(let articles):
                self?.viewModel.articles = articles
                completionHandler(.success(articles))
            case .failure(let error):
                completionHandler(.failure(error))
                }
            }
        }
    }
}
