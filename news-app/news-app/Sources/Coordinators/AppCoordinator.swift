//
//  AppCoordinator.swift
//  news-app
//
//  Created by Stefan Aleksiev on 7.08.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

final class AppCoordinator: Coordinator {
    
    // MARK: - Properties
    
    var childCoordinators = [Coordinator]()
    var navigationController: CustomNavigationController
    
    func start() {
        AppManager.shared.isLoggedIn ? startTabBar() : startLogin()
    }
    
    init(navigationController: CustomNavigationController) {
        self.navigationController = navigationController
    }
    
    func startLogin() {
        let loginCoordinator = LoginCoordinator(navigationController: navigationController)
        childCoordinators.append(loginCoordinator)
        loginCoordinator.start()
    }
    
    func startTabBar() {
        let tabBarCoordinator = TabBarCoordinator(navigationController: navigationController)
        childCoordinators.append(tabBarCoordinator)
        tabBarCoordinator.startWorldNews()
    }
}
