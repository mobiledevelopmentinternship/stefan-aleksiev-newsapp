//
//  RegisterCoordinator.swift
//  news-app
//
//  Created by Stefan Aleksiev on 2.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class RegisterCoordinator: Coordinator {
    
    // MARK: - Delegates
    weak var delegate: BackwardsNavigable?
    
     // MARK: - Properties
    var navigationController: CustomNavigationController
    var childCoordinators = [Coordinator]()
    
    // MARK: - Init
    init(navigationController: CustomNavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        startRegisterCoordinator()
    }
}

// MARK: - Prepare & Start
extension RegisterCoordinator {
    func startRegisterCoordinator() {
        let model = User()
        let viewModel = RegisterViewModel(user: model)
        let registerViewController = RegisterViewController(coordinator: self, viewModel: viewModel)
        registerViewController.coordinator = self
        self.navigationController.pushViewController(registerViewController, animated: true)
    }
    
    func startMainTabCoordinator() {
        AppManager.shared.isLoggedIn = true
        let tabBarCoordinator = TabBarCoordinator(navigationController: navigationController) 
        childCoordinators.append(tabBarCoordinator)
        tabBarCoordinator.start()
    }
    
    func didNavigateBack() {
        delegate?.navigateBack(to: self)
    }
}
