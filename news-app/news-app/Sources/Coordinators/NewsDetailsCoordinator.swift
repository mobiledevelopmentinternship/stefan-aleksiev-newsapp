//
//  NewsDetailsCoordinator.swift
//  news-app
//
//  Created by Stefan Aleksiev on 4.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class NewsDetailsCoordinator: Coordinator {
    
    // MARK: - Delegates
    weak var delegate: BackwardsNavigable?
    
    // MARK: - Properties
    var navigationController: CustomNavigationController
    var childCoordinators = [Coordinator]()
    var viewModel = ArticleDetailsViewModel(article: Article())
    
    // MARK: - Init
    init(navigationController: CustomNavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        startNewsDetailsCoordinator()
    }
}

// MARK: - Prepare & Start
private extension NewsDetailsCoordinator {
    func startNewsDetailsCoordinator() {
        let newsDetailsViewController = NewsDetailsViewController(coordinator: self, viewModel: viewModel)
        newsDetailsViewController.coordinator = self
        navigationController.pushViewController(newsDetailsViewController, animated: true)
    }
}

// MARK: - Delegates
extension NewsDetailsCoordinator {
    func didNavigateBack() {
        delegate?.navigateBack(to: self)
    }
}
