//
//  RegisterCoordinator.swift
//  news-app
//
//  Created by Stefan Aleksiev on 1.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class LoginCoordinator: Coordinator {
    
    // MARK: - Properties
    var navigationController: CustomNavigationController
    var childCoordinators = [Coordinator]()
    
    // MARK: - Init
    init(navigationController: CustomNavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        startLoginCoordinator()
    }
}

// MARK: - Prepare & Start
private extension LoginCoordinator {
    func startLoginCoordinator() {
        let model = User()
        let viewModel = LoginViewModel(user: model)
        let loginViewController = LoginViewController(coordinator: self, viewModel: viewModel)
        loginViewController.coordinator = self
        navigationController.pushViewController(loginViewController, animated: true)
        AppDelegate.shared.setRootViewController(with: loginViewController)
    }
}

// MARK: - Delegates
extension LoginCoordinator {
    func startRegisterCoordinator() {
        let registerCoordinator = RegisterCoordinator(navigationController: navigationController)
        registerCoordinator.delegate = self
        childCoordinators.append(registerCoordinator)
        registerCoordinator.start()
    }
    
    func startTabCoordinator() {
        AppManager.shared.isLoggedIn = true
        let tabBarCoordinator = TabBarCoordinator(navigationController: navigationController)
        childCoordinators.append(tabBarCoordinator)
        tabBarCoordinator.startWorldNews()
    }
}

extension LoginCoordinator: BackwardsNavigable {
    func navigateBack(to: Coordinator) {
        navigationController.popToRootViewController(animated: true)
        childCoordinators.removeLast()
    }
}
