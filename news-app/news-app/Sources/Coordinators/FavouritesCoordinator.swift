//
//  FavouritesCoordinator.swift
//  news-app
//
//  Created by Stefan Aleksiev on 3.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class FavouritesCoordinator: Coordinator {
    
    // MARK: - Properties
    var navigationController: CustomNavigationController
    var childCoordinators = [Coordinator]()
    
    // MARK: - Additional Properties
    private var viewModel = FavouritesViewModel(articles: [])
    
    // MARK: - Init
    init(navigationController: CustomNavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        startFavouritesCoordinator()
    }
    
    func fetchArticles() -> [Article] {
        viewModel.articles = viewModel.fetchArticles()
        return viewModel.articles
    }
}

// MARK: - Prepare & Start
private extension FavouritesCoordinator {
    func startFavouritesCoordinator() {
        let favouritesViewController = FavouriteViewController(coordinator: self, viewModel: viewModel)
        navigationController.pushViewController(favouritesViewController, animated: true)
    }
}

// MARK: - Prepare for NewsDetails
extension FavouritesCoordinator {
    func startNewsDetails(articleModel: Article) {
        let newsDetailsCoordinator = NewsDetailsCoordinator(navigationController: navigationController)
        newsDetailsCoordinator.delegate = self
        newsDetailsCoordinator.viewModel.article = articleModel
        childCoordinators.append(newsDetailsCoordinator)
        newsDetailsCoordinator.start()
    }
}

// MARK: - Delegates
extension FavouritesCoordinator: BackwardsNavigable {
    func navigateBack(to: Coordinator) {
        navigationController.popToRootViewController(animated: true)
        childCoordinators.removeLast()
    }
}
