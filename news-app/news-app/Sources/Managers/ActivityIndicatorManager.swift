//
//  ActivityIndicatorManager.swift
//  news-app
//
//  Created by Stefan Aleksiev on 18.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import NVActivityIndicatorView

final class ActivityIndicatorManager {
    
    // MARK: - Singleton
    static let shared = ActivityIndicatorManager()
    
    private let activityIndicator = NVActivityIndicatorView(
        frame: CGRect(x: 0, y: 0, width: 0, height: 0),
        type: .ballPulse,
        color: .white,
        padding: 25)
    
    private init() {}
    
    // MARK: - Configure and Start
    func start(view: UIView) {
        view.addSubview(activityIndicator)
        activityIndicator.center = view.center
        activityIndicator.startAnimating()
    }
    
    // MARK: - Stop Indicator
    func stop() {
        activityIndicator.stopAnimating()
    }
}
