//
//  NetworkManager.swift
//  news-app
//
//  Created by Stefan Aleksiev on 30.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import Reachability

class Connectivity {

    // MARK: - Singleton
    static  let shared = Connectivity()

    var isConnected: Bool {
        return reachabilityStatus != .none
    }

    var reachabilityStatus: Reachability.Connection = .none
    let reachability = Reachability()!

    private init() {}

    @objc func reachabilityChanged(notification: Notification) {
        let reachability = notification.object as! Reachability

        switch reachability.connection {
        case .none:
            reachabilityStatus = .none
        case .wifi:
            reachabilityStatus = .wifi
        case .cellular:
            reachabilityStatus = .cellular
        }
        NotificationCenter.default.post(name: .didReceiveStatus, object: isConnected)
    }

    func startMonitoring() {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        do {
            try reachability.startNotifier()
        } catch {
            print("Could not start reachability notifier")
        }
    }

    func stopMonitoring() {
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: Notification.Name.reachabilityChanged, object: reachability)
    }
}
