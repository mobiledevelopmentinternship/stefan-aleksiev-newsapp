//
//  AppDelegateConfigurator.swift
//  news-app
//
//  Created by Stefan Aleksiev on 4.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit
import Firebase

final class AppManager {
    
    // MARK: - Singleton
    static let shared = AppManager()
    private let dateFormatter = DateFormatter()
    private let defaults = UserDefaults()
    
    // Keys for UserDefaults
    static let loggedIn = "isLoggedIn"
    
    // MARK: - Defaut value for category & Country
    private (set) var selectedCategory = Category.General
    private (set) var selectedCountry = Country.cz
    
    var isLoggedIn: Bool  {
        get {
            return defaults.bool(forKey: AppManager.loggedIn)
        }
        set {
            defaults.set(newValue, forKey: AppManager.loggedIn)
        }
    }
    private init() {}
}

// MARK: - Setup
extension AppManager {
    func configureTabBar() {
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: Colors.prime], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
    }
    
    func setCategory(_ category: Category) {
        self.selectedCategory = category
    }
    
    func setCountry(_ country: Country) {
        self.selectedCountry = country
    }
}

// MARK: Configure Libraries
extension AppManager {
    func configureFirebase() {
        FirebaseApp.configure()
    }
}

// MARK: - Additional Functions
extension AppManager {
    func getCurrentDate() -> String {
        let date = Date()
        dateFormatter.dateFormat = Constants.DateFormats.todaysDate
        let todaysDate = dateFormatter.string(from: date)
        return todaysDate
    }
    
    func setDateNavigationItem() -> [UIBarButtonItem] {
        // -- TODO: Make them lazy and fix bug --
        let negativeSpacer = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        let dateItem: UIBarButtonItem = {
            let button = UIButton(type: .custom)
            let todaysDate = getCurrentDate()
            
            button.setTitle(todaysDate, for: .normal)
            button.titleLabel?.font = Font.bebasBold.of(size: .semiLarge)
            button.setTitleColor(UIColor.lightGray, for: .normal)
            
            return UIBarButtonItem(customView: button)
        }()
        
        negativeSpacer.width = Constants.NavigationItem.negativeSpacerWidth
        let items = [negativeSpacer, dateItem]
        return items
    }
}
