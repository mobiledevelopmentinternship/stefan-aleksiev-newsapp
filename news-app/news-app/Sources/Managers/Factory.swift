//
//  FactoryItems.swift
//  news-app
//
//  Created by Stefan Aleksiev on 3.08.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

struct Factory {
    
    // MARK: - Navigation
    func setNavigationButtonItem(_ viewController: UIViewController, action: Selector? = nil, iconPath: String) -> UIBarButtonItem {
        let button: UIBarButtonItem = {
            let button = UIButton(type: .custom)
            button.frame = Constants.NavigationItem.frame
            
            let buttonImage = UIImage(named: iconPath)
            button.setImage(buttonImage, for: .normal)
            
            guard let action = action else { return UIBarButtonItem(customView: button) }
            button.addTarget(viewController.self, action: action, for: .touchUpInside)
            
            return UIBarButtonItem(customView: button)
        }()
        return button
    }
    
    // MARK: - Collection view Borders
    func addBorders(_ collectionView: UICollectionView) {
        let widthFrame = collectionView.frame.width
        let multipler = widthFrame / 230
        
        let topBorder = CALayer()
        topBorder.borderColor = UIColor.white.cgColor;
        topBorder.borderWidth = 3
        topBorder.frame = CGRect(x: 0, y: 0, width: widthFrame * multipler, height: 1)
        collectionView.layer.addSublayer(topBorder)
        
        let bottomBorder = CALayer()
        bottomBorder.borderColor = UIColor.white.cgColor;
        bottomBorder.borderWidth = 3
        bottomBorder.frame = CGRect(x: 0, y: collectionView.frame.height, width: widthFrame * multipler, height: 1)
        collectionView.layer.addSublayer(bottomBorder)
    }
    
    // MARK: - Networking Indicator
    func addIndicatorButtonItem(status: Bool) -> UIBarButtonItem {
        let indicatorButton: UIBarButtonItem = {
            let button = UIButton(type: .custom)
            button.frame = Constants.NavigationItem.indicatorFrame
            button.isUserInteractionEnabled = false
            
            var buttonImage = UIImage(named: "")
            
            if status {
                buttonImage = UIImage(named: Constants.NavigationItem.ConnectionIndicator.available)
            } else {
                buttonImage = UIImage(named: Constants.NavigationItem.ConnectionIndicator.unAvailable)
            }
            
            button.setImage(buttonImage, for: .normal)
            return UIBarButtonItem(customView: button)
        }()
        return indicatorButton
    }
}
