//
//  NewsPresenter.swift
//  news-app
//
//  Created by Stefan Aleksiev on 25.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

struct NewsPresenter {
    
    // MARK: - collectionView DataSource & Delegates
    let collectionViewDataSource:  UICollectionViewDataSource?
    let collectionViewDelegate: UICollectionViewDelegate?
    
    // MARK: - collectionView DataSource & Delegates
    let tableViewDataSource:  UITableViewDataSource?
    let tableViewDelegate: UITableViewDelegate?
}

// MARK: - CollectionView Functions
extension NewsPresenter {
    func registerCell<T: UICollectionViewCell>(_ collectionView: UICollectionView, cellType: T.Type) {
        collectionView.registerCell(cellType)
    }
    
    func registerCell<T: UITableViewCell>(_ tableView: UITableView, cellType: T.Type) {
        tableView.registerCell(cellType)
    }
    
    func setupCollectionView(in viewController: UIViewController, collectionView: UICollectionView, anotherCollectionView: UICollectionView) {
        collectionView.dataSource = collectionViewDataSource
        collectionView.delegate = collectionViewDelegate
        
        anotherCollectionView.dataSource = collectionViewDataSource
        anotherCollectionView.delegate = collectionViewDelegate
    }
    
    func setupTableView(in viewController: UIViewController, tableView: UITableView) {
        tableView.dataSource = tableViewDataSource
        tableView.delegate = tableViewDelegate
    }
}
