//
//  CountryTableViewCell.swift
//  news-app
//
//  Created by Stefan Aleksiev on 31.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {

    @IBOutlet private weak var countryImage: UIImageView!
    
    func configure(with imagePath: String) {
        countryImage.image = UIImage(named: imagePath)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5))
    }
}
