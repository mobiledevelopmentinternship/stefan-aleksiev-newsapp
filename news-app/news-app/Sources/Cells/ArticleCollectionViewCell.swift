//
//  NewsCollectionViewCell.swift
//  news-app
//
//  Created by Stefan Aleksiev on 1.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit
import Kingfisher

class ArticleCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet private weak var source: UILabel!
    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var image: UIImageView!
    @IBOutlet private weak var publishAt: UILabel!
    @IBOutlet private weak var readButton: UIButton!
    
    // MARK: - Delegates
    weak var delegate: ReadButtonDelegate?
    
    // MARK: - Properties
    private var article = Article()
    private let repository = RealmManager(repository: RealmClient())
    
    // MARK: - Configure
    func configure(with data: Article) {
        article = data
        source.text = data.source?.name
        title.text = data.title
        publishAt.text = article.publishedAt?.asDateString()
        setFonts()

        guard let image = data.urlToImage else {
            self.image.image = Constants.placeHolderImage
            return
        }
        self.image.kf.setImage(with: URL(string: image), placeholder: Constants.placeHolderImage)
    }
}

// MARK: - Actions
private extension ArticleCollectionViewCell {
    @IBAction func onReadBtnPressed(_ sender: Any) {
        delegate?.readBtnPressed(article)
    }
}

// MARK: - Setup
private extension ArticleCollectionViewCell {
    func setFonts() {
        
        source.font = Font.semiBold.of(size: .semiMedium)
        source.textColor = Colors.prime
        
        publishAt.font = Font.semiBold.of(size: .small)
        publishAt.textColor = Colors.secondComplimentary
        
        title.font = Font.semiBold.of(size: .semiMedium)
        title.textColor = Colors.third
        
        readButton.titleLabel?.font = Font.semiBold.of(size: .semiMedium)
    }
}

// MARK: - Protocols
protocol ReadButtonDelegate: class {
    func readBtnPressed(_ article: Article)
}
