//
//  CategoryCollectionViewCell.swift
//  news-app
//
//  Created by Stefan Aleksiev on 18.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var categoryName: UILabel!
    
    // MARK: - Configure
    func configure(with name: String) {
        if name == Category.General.rawValue {
            categoryName.textColor = Colors.primeComplimentary
        }
        categoryName.font = Font.semiBold.of(size: .medium)
        categoryName.text = name
    }
    
    func setDisabled(with name: String) {
        categoryName.font = Font.semiBold.of(size: .medium)
        categoryName.text = name
    }
}

// MARK: - Additional Functions
extension CategoryCollectionViewCell {
    func getCategoryName() -> String {
        return categoryName?.text ?? Category.General.rawValue
    }
}

// MARK: - Actions
extension CategoryCollectionViewCell {
    
    func didSelect(defaultCell: CategoryCollectionViewCell? = nil, selectedCell: CategoryCollectionViewCell) {
        let selectedCategory = AppManager.shared.selectedCategory
        defaultCell?.categoryName.textColor = Colors.thirdComplimentary
        
        guard let categoryText = categoryName.text, let category = Category(rawValue: categoryText) else {
            return
        }
        
        if selectedCategory.rawValue == category.rawValue {
            selectedCell.categoryName.textColor = Colors.primeComplimentary
            selectedCell.isSelected = false
            return
        } else {
            AppManager.shared.setCategory(category)
            selectedCell.categoryName.textColor = Colors.primeComplimentary
        }
        selectedCell.isSelected = true
    }
    
    func didDeselect() {
        categoryName.textColor = Colors.thirdComplimentary
    }
}
