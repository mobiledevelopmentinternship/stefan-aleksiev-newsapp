//
//  FavouriteCollectionViewCell.swift
//  news-app
//
//  Created by Stefan Aleksiev on 3.08.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit
import Kingfisher

class FavouriteCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet private weak var articleImage: UIImageView!
    @IBOutlet private weak var source: UILabel!
    @IBOutlet private weak var title: UILabel!
    
    // MARK: - Configure
    func configure(with data: Article) {
        self.source.text = data.source?.name ?? ""
        self.title.text = data.title ?? ""
        
        guard let image = data.urlToImage else {
            self.articleImage.image = Constants.placeHolderImage
            return
        }
        
        self.articleImage.kf.setImage(with: URL(string: image), placeholder: Constants.placeHolderImage)
        setFonts()
    }
}

// MARK: - Setup
private extension FavouriteCollectionViewCell {
    func setFonts() {
        source.font = Font.semiBold.of(size: .small)
        source.textColor = Colors.prime
        
        title.font = Font.semiBold.of(size: .semiMedium)
        title.textColor = Colors.third
    }
}
