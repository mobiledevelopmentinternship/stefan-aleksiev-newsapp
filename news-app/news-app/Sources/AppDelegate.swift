//
//  AppDelegate.swift
//  news-app
//
//  Created by Stefan Aleksiev on 1.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    // MARK: - Singleton
    static let shared = AppDelegate()
    
    private var coordinator: AppCoordinator?
    private (set) var rootViewController = UIViewController()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        AppManager.shared.configureFirebase()
        AppManager.shared.configureTabBar()
        configure(application: application)
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        Connectivity.shared.startMonitoring()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        Connectivity.shared.stopMonitoring()
    }
}

// MARK: - Setup Window
private extension AppDelegate {
    func configure(application: UIApplication) {
        let navigationController = CustomNavigationController()
        coordinator = AppCoordinator(navigationController: navigationController)
    
        coordinator?.start()
        rootViewController = AppDelegate.shared.rootViewController
        let isLoggedIn = AppManager.shared.isLoggedIn
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = isLoggedIn ? rootViewController : navigationController
        window?.makeKeyAndVisible()
    }
}

extension AppDelegate {
    func setRootViewController(with viewController: UIViewController) {
        self.rootViewController = viewController
    }
}
