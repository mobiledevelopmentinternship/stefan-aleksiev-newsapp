//
//  Constants.swift
//  news-app
//
//  Created by Stefan Aleksiev on 3.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    static let placeHolderImage = UIImage(named: "image-unavailable.png")
    static let scrollViewМultiplier: CGFloat = 1.7
    
    enum Titles {
        static let worldNews = "World News"
        static let personalNewspaper = "Personal Newspaper"
        static let favourite = "Saved News"
        static let profile = "Profile"
    }
    
    enum Images {
        static let isSaved = UIImage(named: "save-icon-filled")
        static let isNotSaved = UIImage(named: "save-icon.png")
    }
    
    enum Labels {
        static let emptyFavourites = "There is nothing saved yet in your favourites."
        static let unavailableConnection = "There is no internet connection.\n Please try again later."
    }
    
    enum TabBar {
        static let selectedIndex = 1
    
        enum Images {
            static let worldNews = "world-news-icon.png"
            static let worldNewsSelected = "world-news-icon-selected.png"
            
            static let newsPaper = "newspaper-icon.png"
            static let newsPaperSelected = "newspaper-icon-selected.png"
            
            static let favourites = "saved-icon.png"
            static let favouritesSelected = "saved-icon-selected.png"
        }
    }
    
    enum NavigationItem {
        static let frame = CGRect(origin: CGPoint.zero, size: CGSize(width: 50, height: 50))
        static let indicatorFrame = CGRect(origin: CGPoint.zero, size: CGSize(width: 10, height: 10))
        static let negativeSpacerWidth: CGFloat = 25
        
        enum Profile {
            static let icon = "profile-icon.png"
        }
        
        enum Country {
            static let mx = "mx.png"
            static let lv = "lv.png"
            static let ua = "ua.png"
            static let co = "co.png"
            static let eg = "eg.png"
            static let ru = "ru.png"
            static let ro = "ro.png"
            static let cz = "cz.png"
            static let us = "us.png"
            static let bg = "bg.png"
        }
        
        enum Back {
            static let icon = "back-button-icon"
        }
        
        enum BackWithTitle {
            static let icon = "back-button-icon-with-title"
        }
        
        enum ConnectionIndicator {
            static let available = "networking-available.png"
            static let unAvailable = "networking-unavailable.png"
        }
    }
    
    enum ValidationErrors {
        static let email = "Please enter a valid email address."
        static let password = "Invalid password. Your password must contain at least one lowercase, one uppercase and one special character."
        static let repeatPassword = "The password does not match with the one above."
        static let country = "Please choose a country from the dropdown menu."
    }
    
    enum Error {
        static let title = "Unexpected Error."
        static let message = "Problem with fetching data."
        static let saveErrorMessage = "Problem with saving data."
        static let removeErrorMessage = "Problem with removing data."
        static let invalidURL = "Invalid URL"
        static let noInternetConnection = "There is no internet connection."
        
        enum Login {
            static let message = "Wrong username and/or password."
        }
        
        enum Register {
            static let message = "User already exist.\nTry again with different email."
        }
    }

    enum ParallaxHeader {
        static let height: CGFloat = 300
    }
    
    enum DateFormats {
        static let todaysDate = "EEEE, MMM d"
    }
    
    enum Border {
        static let widthMultipler = CGFloat(1.7)
    }
    
    enum Completion {
        static let title = "Success"
        static let updateMessage = "You successfully updated your information."
        static let saveMessage = "You successfully saved the article."
        static let removeMessage = "You successfully removed the article"
    }
    
    enum CollectionView {
        static let contentInset = UIEdgeInsets(top: 0, left: 32, bottom: 0, right: 0)
    }
}
