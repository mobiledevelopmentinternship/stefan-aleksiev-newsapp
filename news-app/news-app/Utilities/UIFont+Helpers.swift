//
//  UIFont+Helpers.swift
//  news-app
//
//  Created by Stefan Aleksiev on 15.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    class func regular(ofSize size: CGFloat) -> UIFont {
        let regular = UIFont(name: "Raleway-Regular", size: size)
        return regular!
    }
}

enum Font: String {
    case bold = "Raleway-Bold"
    case black = "Raleway-Black"
    case light = "Raleway-Light"
    case medium = "Raleway-Medium"
    case regular = "Raleway-Regular"
    case semiBold = "Raleway-SemiBold"
    case extraBold = "Raleway-ExtraBold"
    
    case bebasBold = "BebasNeue-Bold"
    case bebasRegular = "BebasNeue-Regular"
    
    func of(size: FontSize) -> UIFont {
        return UIFont(name: self.rawValue, size: size.rawValue)!
    }
}

enum FontSize: CGFloat {
    case small = 12
    case semiMedium = 14
    case medium = 16
    case semiLarge = 20
    case large = 28
    case extraLarge = 34

}
