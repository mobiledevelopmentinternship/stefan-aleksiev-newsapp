//
//  ViewController+Helpers.swift
//  news-app
//
//  Created by Stefan Aleksiev on 15.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) {
                UIAlertAction in
            }
            alert.addAction(okAction)
            self.present(alert, animated: true)
        }
    }
    
    func showSuccessDialog(title: String, message: String, popVC: Bool) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) {
                UIAlertAction in
                if popVC {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            alert.addAction(okAction)
            self.present(alert, animated: true)
        }
    }
}
