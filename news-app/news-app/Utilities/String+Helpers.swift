//
//  String+Helpers.swift
//  news-app
//
//  Created by Stefan Aleksiev on 17.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

extension String {
    func asDateString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: Date())
    }
}
