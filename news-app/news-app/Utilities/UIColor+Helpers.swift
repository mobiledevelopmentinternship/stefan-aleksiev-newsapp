//
//  UIColor+Helpers.swift
//  news-app
//
//  Created by Stefan Aleksiev on 3.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(hex: Int) {
        let components = (
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        self.init(red: components.R, green: components.G, blue: components.B, alpha: 1)
    }
}

extension CGColor {
    class func colorWithHex(hex: Int) -> CGColor {
        return UIColor(hex: hex).cgColor
    }
}

enum Colors {
    static let prime = UIColor(hex: 0x0076FF)
    static let primeComplimentary = UIColor(hex: 0xEEA053)
    static let second = UIColor(hex: 0xA60F0F)
    static let secondComplimentary = UIColor(hex: 0x9A9A9A)
    static let third = UIColor(hex: 0x000000)
    static let thirdComplimentary = UIColor(hex: 0xFFFFFF)
}
