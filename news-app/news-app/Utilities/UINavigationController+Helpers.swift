//
//  UINavigationController+Helpers.swift
//  news-app
//
//  Created by Stefan Aleksiev on 16.07.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit
import Foundation

enum Margin: CGFloat {
    case left = 32
}
