//
//  Notification+Helpers.swift
//  news-app
//
//  Created by Stefan Aleksiev on 1.08.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import UIKit

extension Notification.Name {
    static let didReceiveStatus = Notification.Name("didReceiveStatus")
}
